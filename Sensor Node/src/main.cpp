#include <Arduino.h>
#include "ros.h"
#include <sensor_msgs/Range.h>
#include <Ultrasonic.h>

#define US0 2
#define US1 3
#define US2 4
#define US3 5
#define US4 6
#define US5 7

/**
 * ROS Node Handle
 * Needed for communication over rosserial
 */
ros::NodeHandle nh;

/**
 * Helper Funtions to better log text
 * @param debug Text to log
 */
void logdebug(String debug) {
  char buf[160];
  debug.toCharArray(buf, 160);
  nh.logdebug(buf);
}

void loginfo(String info) {
  char buf[160];
  info.toCharArray(buf, 160);
  nh.loginfo(buf);
}

/**
 * Define Publisher
 * ----
 * First define Message
 * Then define Publisher
 *  Parameters:
 *    - Topic name
 *    - Message that will be published
*/
sensor_msgs::Range ultrasonic0_msg;
sensor_msgs::Range ultrasonic1_msg;
sensor_msgs::Range ultrasonic2_msg;
sensor_msgs::Range ultrasonic3_msg;
sensor_msgs::Range ultrasonic4_msg;
sensor_msgs::Range ultrasonic5_msg;

ros::Publisher ultrasonic0_pub("ultrasonic/us0", &ultrasonic0_msg);
ros::Publisher ultrasonic1_pub("ultrasonic/us1", &ultrasonic1_msg);
ros::Publisher ultrasonic2_pub("ultrasonic/us2", &ultrasonic2_msg);
ros::Publisher ultrasonic3_pub("ultrasonic/us3", &ultrasonic3_msg);
ros::Publisher ultrasonic4_pub("ultrasonic/us4", &ultrasonic4_msg);
ros::Publisher ultrasonic5_pub("ultrasonic/us5", &ultrasonic5_msg);

Ultrasonic ultrasonic0(US0);
Ultrasonic ultrasonic1(US1);
Ultrasonic ultrasonic2(US2);
Ultrasonic ultrasonic3(US3);
Ultrasonic ultrasonic4(US4);
Ultrasonic ultrasonic5(US5);

sensor_msgs::Range distanceToRange(int dist) {
  sensor_msgs::Range intern;
  intern.header.stamp = nh.now();
  intern.min_range = 0.02;
  intern.max_range = 3;
  intern.field_of_view = 30 * PI/180;
  intern.range = dist/100;
  return intern;
}

void setup() {
  nh.initNode();

  /**
   * Init all Subscribers and Publishers
   * Advertise each and every Publisher with it's variable name
   */
  //nh.advertise(somePublisher)
  //nh.subscribe(someSubscriber)
  nh.advertise(ultrasonic0_pub);
  nh.advertise(ultrasonic1_pub);
  nh.advertise(ultrasonic2_pub);
  nh.advertise(ultrasonic3_pub);
  nh.advertise(ultrasonic4_pub);
  nh.advertise(ultrasonic5_pub);

  /**
   * Wait until ROS is connected
   */
  while (!nh.connected())
  {
    nh.spinOnce();
  }
}

void loop() {
  nh.spinOnce();
  int dist;

  dist = ultrasonic0.read();
  ultrasonic0_msg = distanceToRange(dist);
  ultrasonic0_pub.publish(&ultrasonic0_msg);

  dist = ultrasonic1.read();
  ultrasonic1_msg = distanceToRange(dist);
  ultrasonic1_pub.publish(&ultrasonic1_msg);

  dist = ultrasonic2.read();
  ultrasonic2_msg = distanceToRange(dist);
  ultrasonic2_pub.publish(&ultrasonic2_msg);

  dist = ultrasonic3.read();
  ultrasonic3_msg = distanceToRange(dist);
  ultrasonic3_pub.publish(&ultrasonic3_msg);

  dist = ultrasonic4.read();
  ultrasonic4_msg = distanceToRange(dist);
  ultrasonic4_pub.publish(&ultrasonic4_msg);

  dist = ultrasonic5.read();
  ultrasonic5_msg = distanceToRange(dist);
  ultrasonic5_pub.publish(&ultrasonic5_msg);
}